<!DOCTYPE html>
<html lang="en">
@include('layouts.landing.head', ['title' => 'Registrasi Bukit Jabal'])
<body>

@include('layouts.landing.header', ['page' => 'registrasi'])

    <div id="hero" class="hero overlay subpage-hero sop-hero">
        <div class="hero-content">
            <div class="hero-text">
                <h1>Registrasi Data</h1>
            </div><!-- /.hero-text -->
        </div><!-- /.hero-content -->
    </div><!-- /.hero -->

    <main id="main" class="site-main" style="margin: auto; padding: 1%;">
        <div class="container text-center">
            <div class="card my-5 py-5">
                <div class="card-body m-5">
                    <h2>Registrasi</h2>
                    <div id="peserta_collection">
                        <div class="row mt-5 mx-5">
                            <div class="col-2 my-auto">Nama Kelompok <br><br></div>
                            <div class="col">
                                <div class="row">
                                    <div class="col text-left">
                                        <input id="input_validasi" type="text" class="form-control border" placeholder="Masukkan Nama Kelompok" oninput="cek_nama_kel(this)" spellcheck="false">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <small id="ket_validasi">Cek Nama Kelompok</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="form1" class="border m-5">
                            <div class="m-5 px-5 text-left">
                                <div class="row mt-4">
                                    <div class="col-2 my-auto">
                                        <span class="badge bg-dark"><h4 class="text-white"><b>Ketua</b></h4></span>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-2 my-auto">Nama</div>
                                    <div class="col"><input type="text" class="form-control border" placeholder="Masukkan Nama"></div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-2 my-auto">Nomor HP</div>
                                    <div class="col"><input type="text" class="form-control border" placeholder="Masukkan Nomor HP"></div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-2 my-auto">Riwayat Penyakit</div>
                                    <div class="col">
                                        <input type="text" class="form-control border" placeholder="Masukkan Riwayat Penyakit">
                                        <small class="text-muted">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jika memiliki Riwayat Penyakit lebih dari 1, pisahkan dengan tanda koma <b>,</b> <br>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Contoh : Asma,Hipotensi,Hipotermia</i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-4 mb-4">
                        <div class="col text-left">
                            <input type="hidden" id="total_peserta_num" value="1">
                            Total Peserta : <span id="total_peserta">1</span>
                        </div>
                    </div>
                    <div class="row mx-4">
                        <div class="col text-left">
                            <button type="button" class="btn btn-primary border" id="add_peserta_btn"><i class="fa fa-user-plus mr-2"></i> Tambah Peserta</button>
                        </div>
                        <div class="col text-right">
                            <button type="submit" class="btn btn-primary border"><i class="fa fa-send mr-2"></i> Daftarkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@include('layouts.landing.footer')

@include('layouts.landing.scripts')
<script>
    let id=1;

	function cek_nama_kel(_this) {
		let kelompok = $(_this).val();
        $('#ket_validasi').html($('#ket_validasi').text()+' <i class="fa fa-spinner fa-spin"></i>')

		if (kelompok) {
            // if (kelompok !== '') {
                $.ajax({
                    url: "{{ route('registrasi.cek_nama_kel') }}",
                    type: 'GET',
                    data: {
                        kelompok: kelompok
                    },
                    success: function (data) {
                        if ($.trim(data)){
                            if (data === 0) {
                                $('#input_validasi').attr('class', 'form-control border-success')
                                $('#ket_validasi').attr('class', 'text-success')
                                $('#ket_validasi').text('Nama Kelompok Tersedia')
                            } else {
                                $('#input_validasi').attr('class', 'form-control border-danger')
                                $('#ket_validasi').attr('class', 'text-danger')
                                $('#ket_validasi').text('Nama Kelompok Sudah Terpakai')
                            }
                        }
                        if (kelompok.length == 0) {
                            $('#input_validasi').attr('class', 'form-control border-danger')
                            $('#ket_validasi').attr('class', 'text-danger')
                            $('#ket_validasi').text('Nama Kelompok Sudah Terpakai')
                        }
                    },
                });
		} else {
            $('#input_validasi').attr('class', 'form-control border')
            $('#ket_validasi').attr('class', '')
            $('#ket_validasi').text('Cek Nama Kelompok')
        }
	}

    $('#add_peserta_btn').click(function () {
		id++
		$('#peserta_collection').append(
            '<div id="form'+id+'" class="border m-5" style="display:none">' +
            '<div class="m-5 px-5 text-left">' +
            '<div class="row mt-4">' +
            '<div class="col-2 my-auto">' +
            '<span class="badge bg-dark"><h4 class="text-white"><b>Anggota</b></h4></span>' +
            '</div>' +
            '<div class="col text-right"><button type="button" class="btn-close" data-id="' + id + '" onclick="remove_peserta(this)"></button></div>' +
            '</div>' +
            '<div class="row mt-4">' +
            '<div class="col-2 my-auto">Nama</div>' +
            '<div class="col"><input type="text" class="form-control border" placeholder="Masukkan Nama"></div>' +
            '</div>' +
            '<div class="row mt-4">' +
            '<div class="col-2 my-auto">Nomor HP</div>' +
            '<div class="col"><input type="text" class="form-control border" placeholder="Masukkan Nomor HP"></div>' +
            '</div>' +
            '<div class="row mt-4">' +
            '<div class="col-2 my-auto">Riwayat Penyakit</div>' +
            '<div class="col">' +
            '<input type="text" class="form-control border" placeholder="Masukkan Riwayat Penyakit">' +
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<small class="text-muted">Jika memiliki Riwayat Penyakit lebih dari 1, pisahkan dengan tanda koma <b>,</b></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
		)
		$('#form' + id).slideDown()
        $('#total_peserta_num').val(parseInt($('#total_peserta_num').val())+1);
        $('#total_peserta').html($('#total_peserta_num').val());
	});

    function remove_peserta(_this) {
		let id = $(_this).data('id')
		$('#form' + id).slideUp("normal", function() { $(this).remove(); } )
        $('#total_peserta_num').val(parseInt($('#total_peserta_num').val())-1);
        $('#total_peserta').html($('#total_peserta_num').val());
	}
</script>
</body>
</html>