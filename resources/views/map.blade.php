<!DOCTYPE html>
<html lang="en">
@include('layouts.landing.head', ['title' => 'Map Bukit Jabal'])
<body>

@include('layouts.landing.header', ['page' => 'map'])

    <div id="hero" class="hero overlay subpage-hero sop-hero">
        <div class="hero-content">
            <div class="hero-text">
                <h1>Map</h1>
            </div><!-- /.hero-text -->
        </div><!-- /.hero-content -->
    </div><!-- /.hero -->

    <main id="main" class="site-main">

        <section class="site-section subpage-site-section section-rute-us">

            <div class="container">
                <div class="col">
                    <div class="col-sm-6">
                        <div class="rute-info">
                            <h2>Alamat</h2>
                            <h3>Address</h3>
                                <ul class="list-unstyled">
                                    <li>Jl. Kakatua No 35 RT 15 RW 07 Dusun Sumberbendo Desa Kucur.</li>
                                </ul>
                            <h3>Google Map</h3>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.432714616008!2d112.53936941477916!3d-7.954155494270849!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e788390d7848ba9%3A0xcd62c1e656b06af5!2sBasecamp%20pendakian%20Bukit%20Jabal!5e0!3m2!1sid!2sid!4v1649819166801!5m2!1sid!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            <!-- <a href="https://maps.app.goo.gl/niUjyWP5gwwHCR7g7" target="_blank">maps.app.goo.gl/niUjyWP5gwwHCR7g7</a> -->
                        </div><!-- /.rute-info -->
                    </div>

                    <div class="col-sm-6">
                        <div class="rute-info">
                            <h2>Nomor Whatsapp</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Informasi</h3>
                                    <a href="https://wa.me/082333665766" target="_blank">082333665766</a>
                                    <h3>Informasi</h3>
                                    <a href="https://wa.me/081330758431" target="_blank">081330758431</a>
                                    <h3>Informasi</h3>
                                    <a href="https://wa.me/08884863962" target="_blank">08884863962</a>
                                    <h3>Keamanan dan SAR</h3>
                                    <a href="https://wa.me/089526480998" target="_blank">089526480998</a>
                                    <h3>Keamanan dan SAR</h3>
                                    <a href="https://wa.me/082131186738" target="_blank">082131186738</a>
                                </div>
                            </div>
                        </div><!-- /.rute-info -->
                    </div>
                </div>
            </div>
            
        </section><!-- /.section-rute-us -->

    </main><!-- /#main -->

@include('layouts.landing.footer')

@include('layouts.landing.scripts')
</body>
</html>