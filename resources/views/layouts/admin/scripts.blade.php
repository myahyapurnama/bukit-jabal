<script data-cfasync="false" src="{{ asset('js/admin/email-decode.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery-ui.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/popper.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script>

<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/waves.min.js') }}"></script>

<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery.slimscroll.js') }}"></script>

<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery.flot.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery.flot.categories.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/curvedlines.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/jquery.flot.tooltip.min.js') }}"></script>

<script src="{{ asset('js/admin/chartist.js') }}" type="d2d1d6e2f87cbebdf4013b26-text/javascript"></script>

<script src="{{ asset('js/admin/amcharts.js') }}" type="d2d1d6e2f87cbebdf4013b26-text/javascript"></script>
<script src="{{ asset('js/admin/serial.js') }}" type="d2d1d6e2f87cbebdf4013b26-text/javascript"></script>
<script src="{{ asset('js/admin/light.js') }}" type="d2d1d6e2f87cbebdf4013b26-text/javascript"></script>

<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/pcoded.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/vertical-layout.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/custom-dashboard.min.js') }}"></script>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript" src="{{ asset('js/admin/script.min.js') }}"></script>
<script data-cf-settings="d2d1d6e2f87cbebdf4013b26-|49" defer="" src="{{ asset('js/admin/rocket-loader.min.js') }}"></script>