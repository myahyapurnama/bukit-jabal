{{-- Font --}}
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

{{-- Bootstrap --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/bootstrap.min.css') }}">

{{-- Effect --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/waves.min.css') }}" media="all">

{{-- Icons --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/feather.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/themify-icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/icofont.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/font-awesome-n.min.css') }}">

{{-- Chart --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/chartist.css') }}" media="all">

{{-- Style --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/pages.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/widget.css') }}">