<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="pcoded-navigation-label">Menu</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu">
                    <li class="{{ ($title == 'Beranda Admin - Bukit Jabal') ? 'active' : '' }}">
                        <a href="navbar-light.html" class="waves-effect waves-dark">
                            <span class="pcoded-micon">
                                <i class="feather icon-home"></i>
                            </span>
                            <span class="pcoded-mtext">Beranda</span>
                        </a>
                    </li>
                </li>
                <li class="pcoded-hasmenu {{ (str_contains($title, 'Pengaturan Halaman')) ? 'active pcoded-trigger' : '' }}">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-globe"></i>
                        </span>
                        <span class="pcoded-mtext">Pengaturan Halaman</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="{{ ($title == 'Home - Pengaturan Halaman') ? 'active' : '' }}">
                            <a href="{{ url('admin/halaman/home') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Home</span>
                            </a>
                        </li>
                        {{-- <li class="">
                            <a href="breadcrumb.html" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Breadcrumbs</span>
                            </a>
                        </li> --}}
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-users"></i>
                        </span>
                        <span class="pcoded-mtext">Data Pendaftar</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="alert.html" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Alert</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>