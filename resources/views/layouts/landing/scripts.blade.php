<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="{{ asset('js/bootstrap5/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/landing/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/landing/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/landing/jquery.slicknav.min.js') }}"></script>
<script src="{{ asset('js/landing/jquery.countTo.min.js') }}"></script>
<script src="{{ asset('js/landing/jquery.shuffle.min.js') }}"></script>
<script src="{{ asset('js/landing/script.js') }}"></script>