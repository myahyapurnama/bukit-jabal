<footer id="colophon" class="site-footer">
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-8">
                    <div class="social-links">
                        <div><a class="instagram" href="https://www.instagram.com/bukit_jabal_malang/"><i class="fa fa-instagram"></i> bukit_jabal_malang</a></div>
                        <div><a class="whatsapp" href="https://wa.me/082333665766"><i class="fa fa-whatsapp"></i> 082333665766</a></div>
                        <div><a class="whatsapp" href="https://wa.me/081330758431"><i class="fa fa-whatsapp"></i> 081330758431</a></div>
                    </div><!-- /.social-links -->
                </div>
                <div class="col-xs-4">
                    <div class="text-right">
                        <img src="assets/img/favicon.png" alt="" style="height: 70px;">
                        <p>&copy; Bukit Jabal</p>
                        <p><a href="{{ url('login') }}"><i class="fa fa-cog"></i></a> All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.copyright -->
</footer><!-- /#footer -->