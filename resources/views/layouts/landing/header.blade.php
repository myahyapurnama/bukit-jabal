<header id="masthead" class="site-header">
    <nav id="primary-navigation" class="site-navigation">
        <div class="container">

            <div class="navbar-header">
               
                <a class="site-title"><img src="{{ asset('assets/img/favicon.png') }}" alt="icon" width="25px">&nbsp; <span>Bukit</span>Jabal</a>

            </div><!-- /.navbar-header -->

            <div class="collapse navbar-collapse" id="Bukit-navbar-collapse">

                <ul class="nav navbar-nav navbar-right">

                    <li {{ ($page == 'home') ? 'class=active' : '' }}><a href="{{ url('/') }}">Home</a></li>
                    <li {{ ($page == 'sop') ? 'class=active' : '' }}><a href="{{ url('sop') }}">SOP</a></li>
                    <li {{ ($page == 'rute') ? 'class=active' : '' }}><a href="{{ url('rute') }}">Rute</a></li>
                    <li {{ ($page == 'contact') ? 'class=active' : '' }}><a href="{{ url('map') }}">Contact & Map</a></li>
                    <li>|</li>
                    <li {{ ($page == 'registrasi') ? 'class=active' : '' }}><a href="{{ url('registrasi') }}">Registrasi</a></li>

                </ul>

            </div>

        </div>   
    </nav><!-- /.site-navigation -->
</header><!-- /#mastheaed -->