<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login Admin</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Favicon
    ================================================== -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon.png') }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/admin/waves.min.css') }}" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/feather.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/themify-icons.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/icofont.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/font-awesome.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/pages.css') }}">
</head>

<body themebg-pattern="theme1">

    <section class="login-block">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <form class="md-float-material form-material" action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">Login Admin</h3>
                                    </div>
                                </div>
                                @if(session('error'))
                                    <div class='alert alert-danger text-center' role='alert'>
                                        <b>
                                            <i class="fa fa-exclamation-circle"></i> {{ session('error') }}
                                        </b>
                                    </div>
                                @else
                                    <div class='alert alert-success text-center' role='alert'>
                                        <b>
                                            <i class="fa fa-info-circle"></i> Silahkan masukkan Username dan Password
                                        </b>
                                    </div>
                                @endif
                                <div class="form-group form-primary">
                                    <input type="text" name="username" class="form-control" required>
                                    <span class="form-bar"></span>
                                    <label class="float-label">Username</label>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="password" class="form-control" required>
                                    <span class="form-bar"></span>
                                    <label class="float-label">Password</label>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"><i class="fa fa-sign-in mr-2"></i> LOGIN</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

        </div>

    </section>


    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/jquery.min.js') }}"></script>
    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/jquery-ui.min.js') }}"></script>
    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/popper.min.js') }}"></script>
    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/admin/waves.min.js') }}" type="4878d7dfa7bc22a8dfa99416-text/javascript"></script>

    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/jquery.slimscroll.js') }}"></script>

    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/modernizr.js') }}"></script>
    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/css-scrollbars.js') }}"></script>
    <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset('js/admin/common-pages.js') }}"></script>
    
    <script src="{{ asset('js/admin/rocket-loader.min.js') }}" data-cf-settings="4878d7dfa7bc22a8dfa99416-|49" defer=""></script>
</body>

</html>
