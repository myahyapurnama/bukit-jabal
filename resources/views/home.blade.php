@extends('layouts.admin.app', ['title' => 'Beranda Admin - Bukit Jabal'])
@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="feather icon-home bg-c-yellow"></i>
                <div class="d-inline">
                    <h5>Beranda</h5>
                    <span>Selamat Datang di Halaman Admin</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class=" breadcrumb breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/') }}"><i class="feather icon-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">Beranda Admin</li>
                </ul>
            </div>
        </div>
    </div>
</div>

{{-- <div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">

                <div class="row">

                    <div class="col-md-12 col-xl-12">
                        <div class="card sale-card">
                            <div class="card-header">
                                <h5>Deals Analytics</h5>
                            </div>
                            <div class="card-block">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div> --}}
@endsection