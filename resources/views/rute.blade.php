<!DOCTYPE html>
<html lang="en">
@include('layouts.landing.head', ['title' => 'Rute Bukit Jabal'])
<body>

@include('layouts.landing.header', ['page' => 'rute'])

    <div id="hero" class="hero overlay subpage-hero sop-hero">
        <div class="hero-content">
            <div class="hero-text">
                <h1>Rute</h1>
            </div><!-- /.hero-text -->
        </div><!-- /.hero-content -->
    </div><!-- /.hero -->

    <main id="main" class="site-main" style="margin: auto; padding: 1%;">
        <div class="container text-center">
            <img src="{{ asset('assets/img/rute.jpg') }}" alt="" style="max-width: 400px">
        </div>
    </main>

@include('layouts.landing.footer')

@include('layouts.landing.scripts')
</body>
</html>