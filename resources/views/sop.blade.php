<!DOCTYPE html>
<html lang="en">
@include('layouts.landing.head', ['title' => 'SOP Bukit Jabal'])
<body>

@include('layouts.landing.header', ['page' => 'sop'])

    <div id="hero" class="hero overlay subpage-hero sop-hero">
        <div class="hero-content">
            <div class="hero-text">
                <h1>Standar Operasional Prosedur</h1>
            </div><!-- /.hero-text -->
        </div><!-- /.hero-content -->
    </div><!-- /.hero -->

    <main id="main" class="site-main">

        <section class="site-section section-ui-elements">
            <div class="container">
                <div class="selectpicker-checkboxes-radiobtn mt-50">
                    <h2 class="text-center heading-separator">Kedatangan Pendaki</h2>
                    <div class="col">
                        <div class="col-sm-4 mt-20">
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">Lapor ke basecamp</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox2">
                                <label for="checkbox2">Mengisi formulir pendakian dan administrasi</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox3">
                                <label for="checkbox3">Menuju lokasi penitipan kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox4">
                                <label for="checkbox4">Mengikuti safety brefing</label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.selectpicker-checkboxes-radiobtn -->
            </div>

            <div class="container">
                <div class="selectpicker-checkboxes-radiobtn mt-50">
                    <h2 class="text-center heading-separator">Melakukan Pendakian</h2>
                    <div class="col">
                        <div class="col-sm-4 mt-20">
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">Menjaga ketenangan selama di kampung</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox2">
                                <label for="checkbox2">Tidak merusak property masyarakat</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox3">
                                <label for="checkbox3">Tidak buang sampah sembarangan</label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.selectpicker-checkboxes-radiobtn -->
            </div>

            <div class="container">
                <div class="selectpicker-checkboxes-radiobtn mt-50">
                    <h2 class="text-center heading-separator">Selama di Puncak</h2>
                    <div class="col">
                        <div class="col-sm-4 mt-20">
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">Mengikuti aturan yang telah dibuat</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox2">
                                <label for="checkbox2">Mendirikan tenda pada tempat yang telah ditentukan</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox3">
                                <label for="checkbox3">Tidak membuat api unggun</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox4">
                                <label for="checkbox4">Tidak buang sampah sembarangan</label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.selectpicker-checkboxes-radiobtn -->
            </div>

            <div class="container">
                <div class="selectpicker-checkboxes-radiobtn mt-50">
                    <h2 class="text-center heading-separator">Waktu Turun</h2>
                    <div class="col">
                        <div class="col-sm-4 mt-20">
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">Bersihkan dan bawa turun sampah pendakian</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox2">
                                <label for="checkbox2">Pastikan tidak ada benda yang bisa menimbulkan kebakaran</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox3">
                                <label for="checkbox3">Ikuti jalur turun yang sudah ada jangan buat jalur baru</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="checkbox4">
                                <label for="checkbox4">Tidak buang sampah sembarangan</label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.selectpicker-checkboxes-radiobtn -->
            </div>
        </section><!-- /.section-ui-elements -->

    </main><!-- /#main -->

@include('layouts.landing.footer')

@include('layouts.landing.scripts')
</body>
</html>