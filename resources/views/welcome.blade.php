<!DOCTYPE html>
<html lang="en">
@include('layouts.landing.head', ['title' => 'Bukit Jabal'])
<body>

@include('layouts.landing.header', ['page' => 'home'])

    <div id="hero" class="hero overlay">
        <div class="hero-content">
            <div class="hero-text">
                <h1 class="text-white">Bukit Jabal</h1>
                <p class="text-white">Lereng Gunung di Dusun Sumber Bendo, Desa Kucur, Kecamatan, Dau, Malang.</p>
                <a href="map.html" class="btn btn-border">Lokasi Map</a>
            </div><!-- /.hero-text -->
        </div><!-- /.hero-content -->
    </div><!-- /.hero -->

    <main id="main" class="site-main">

        <section class="site-section section-features">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <h2>Bukit Jabal</h2>
                        <p>Bukit Jabal ini sendiri merupakan bukit yang dikelola oleh masyarakat sekitar. Kalian ingin menikmati keindahannya juga? Yuk segera datang ke petugas basecamp dan mengisi formulir.
                        Setelah mengisi formulir, langkah selanjutnya pengunjung melakukan pembayaran donasi lingkungan dan penitipan kendaraan.</p>
                        <p>Registrasi dibuka hari Sabtu pukul 08:00-22:00 dan hari Minggu pukul 08:00-20:00 bertempat di poskamling setelah gapura pintu masuk Dusun Sumberbendo.
                        Selain hari Sabtu Minggu dan jam piket  registrasi dan pengambilan KTP/SIM di sekretariat. Alamat Sekretariat : jl kakatua no 35 RT 15 RW 07 Dusun Sumberbendo Desa Kucur.</p>
                    </div>
                    <br>
                    <br>
                    <div class="col-sm-7 hidden-xs">
                        <img src="assets/img/ipad-pro.png" alt="" class="shadow bg-body rounded">
                    </div>
                </div>
            </div>
        </section><!-- /.section-features -->
        
    </main><!-- /#main -->

@include('layouts.landing.footer')

@include('layouts.landing.scripts')
</body>
</html>