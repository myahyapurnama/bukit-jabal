<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('sop', function(){
    return view('sop');
});

Route::get('rute', function(){
    return view('rute');
});

Route::get('map', function(){
    return view('map');
});

Route::get('registrasi', function(){
    return view('registrasi');
});
    
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/admin/halaman/home', function(){
        return view('pengaturan_halaman.home');
    });
});

Route::group(['middleware' => ['guest']], function () {

});

Route::get('registrasi/cek_nama_kel', 'PesertaController@cek_nama_kel')->name('registrasi.cek_nama_kel');

Auth::routes();
