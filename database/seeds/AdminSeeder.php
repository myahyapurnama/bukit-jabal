<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name'	=> 'Admin',
            'level'	=> 'Admin',
            'username'	=> 'admin',
            'email'	=> 'adminbukitjabal@gmail.com',
            'password'	=> Hash::make('admin'),
        ]);
    }
}
