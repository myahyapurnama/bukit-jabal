<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PesertaController extends Controller
{
    public function cek_nama_kel(Request $request)
    {
        // Validasi Jenis Dokumen (Sertifikat)
        $data = DB::table('pendaftars')
            ->where('kelompok', $request->kelompok)
            ->count();

        return response()->json($data);
    }
}
